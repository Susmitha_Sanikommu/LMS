from flask import Flask, render_template, url_for, request, session, redirect
from flask.ext.pymongo import PyMongo
import bcrypt
from flask import jsonify

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'mongologinexample'
app.config['MONGO_URI'] = 'mongodb://Aakanksha:lms123@ds111618.mlab.com:11618/mysite'


mongo = PyMongo(app)

@app.route('/')
def index():
    #if 'username' in session:
    return render_template('index.html')
    #return render_template('login.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        users = mongo.db.users
        login_user = users.find_one({'name' : request.form['username']})

        if request.form['username'] == "vidya" :
            return render_template('instructor.html')

        if login_user:
            hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
            if bcrypt.hashpw(bytes(request.form['pass'], 'utf-8'), hashpass) == hashpass:
                session['username'] = request.form['username']
                return render_template('CourseHomePage.html')
        return 'Invalid username/password combination'

    else:
        return render_template('login.html')

@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        users = mongo.db.users
        existing_user = users.find_one({'name' : request.form['username']})

        if existing_user is None:
            hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
            users.insert({'name' : request.form['username'], 'password' : hashpass})
            session['username'] = request.form['username']
            return render_template('mypage.html')
        return 'That username already exists!'
    return render_template('register.html')

@app.route('/course_request', methods=['POST', 'GET'])
def course_request():
    if request.method == 'POST':
        #users = mongo.db.users
        cr = mongo.db.courseRequests
        existing_user = cr.find_one({"name" : "sush"})
        existing_user['send_request'] = '1'
        cr.save(existing_user)
        return render_template('ML_RequestStatus.html',result = existing_user)

@app.route('/course_status', methods=['POST', 'GET'])
def course_status():
    if request.method == 'POST':
        #users = mongo.db.users
        cr = mongo.db.courseRequests
        existing_user = cr.find_one({"name" : "sush"})
        #existing_user['accept_request'] = '1'
        #cr.save(existing_user)
        #if existing_user is None:
        #    hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
        #    users.insert({'name' : request.form['username'], 'password' : hashpass})
        #    session['username'] = request.form['username']
        #    return render_template('mypage.html')
        #return 'Request send to the instructor succesfully'
        return render_template('CourseStatus.html',student = existing_user)
    #return render_template('course_status.html')

@app.route('/course_accept', methods=['POST', 'GET'])
def course_accept():
    if request.method == 'POST':
        #users = mongo.db.users
        cr = mongo.db.courseRequests
        existing_user = cr.find_one({"name" : "sush"})
        existing_user['accept_request'] = "1"
        cr.save(existing_user)
        #if existing_user is None:
        #    hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
        #    users.insert({'name' : request.form['username'], 'password' : hashpass})
        #    session['username'] = request.form['username']
        #    return render_template('mypage.html')
        #return 'Request send to the instructor succesfully'
        if existing_user['accept_request'] == "1" :
            return render_template('StudentAccepted.html',student_status = existing_user)
    #return render_template('course_status.html')

@app.route('/myprofile')
def myprofile():
    users = mongo.db.users
    existing_user = users.find_one({'name' : request.form['username']})
    return render_template('myprofile.html',existing_user=existing_user)

@app.route('/courses')
def courses():
    return render_template('courses.html')

@app.route('/StudentCourses')
def StudentCourses():
    return render_template('StudentCourses.html')

@app.route('/WebTechnologies')
def WebTechnologies():
    return render_template('WebTechnologies.html')

@app.route('/MachineLearning')
def MachineLearning():
    return render_template('MachineLearning.html')

@app.route('/ml',methods=['POST', 'GET'])
def ml():
    if request.method == 'POST':
        return render_template('ml.html')

@app.route('/Python')
def Python():
    return render_template('Python.html')

@app.route('/ArtificialIntelligence')
def ArtificialIntelligence():
    return render_template('ArtificialIntelligence.html')



@app.route('/logout')
def logout():
   # remove the username from the session if it is there
   session.pop('username', None)
   return redirect(url_for('index'))
if __name__ == '__main__':
    app.secret_key = 'mysecret'
app.run(debug=True)
